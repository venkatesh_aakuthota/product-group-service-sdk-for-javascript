import ProductGroupServiceSdkConfig from './productGroupServiceSdkConfig';
import DiContainer from './diContainer';
import ProductGroupSynopsisView from './productGroupSynopsisView';
import ListProductGroupsFeature from './listProductGroupsFeature';

/**
 * @class {ProductGroupServiceSdk}
 */
export default class ProductGroupServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {ProductGroupServiceSdkConfig} config
     */
    constructor(config:ProductGroupServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * @param {string} accessToken
     * @returns {Array<ProductGroupSynopsisView>}
     */
    listProductGroups(accessToken:string):Array<ProductGroupSynopsisView> {

        return this
            ._diContainer
            .get(ListProductGroupsFeature)
            .execute(accessToken);

    }

}
