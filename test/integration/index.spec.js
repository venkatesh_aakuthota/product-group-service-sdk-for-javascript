import ProductGroupServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be ProductGroupServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new ProductGroupServiceSdk(config.productGroupServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(ProductGroupServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listProductGroups method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ProductGroupServiceSdk(config.productGroupServiceSdkConfig);


                /*
                 act
                 */
                const productGroupsPromise =
                    objectUnderTest
                        .listProductGroups(
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                productGroupsPromise
                    .then((productGroups) => {
                        expect(productGroups.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000)
        });
    });
});